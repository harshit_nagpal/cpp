// This program was created by Harshit Nagpal
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    // Loops in C++:
    // There are three types of loops in C++:
    //    1. For loop
    //    2. While Loop
    //    3. do-While Loop

    // For loop
    // Syntax of for loop:
    // for(initialization; condition; updation)
    // {
    //     loop body(C++ code);
    // }

    // for (int i = 1; i <= 40; i++)
    // {
    //     cout << i << endl;
    // }

    // Example of infinite for loop
    // for (int i = 1; 34 <= 40; i++)
    // {
    //     cout << i << endl;
    // }

    // While loop
    // Syntax:
    // while(condition)
    // {
    //     C++ statements;
    // }

    // Printing 1 to 40 using while loop
    // int i = 1;
    // while (i <= 40)
    // {
    //     cout << i << endl;
    //     i++;
    // }

    // Example of infinite while loop
    // int i = 1;
    // while (true)
    // {
    //     cout << i << endl;
    //     i++;
    // }

    // do While loop in C++
    // Syntax:
    // do
    // {
    //     C++ statements;
    // }while(condition);

    // Printing 1 to 40 using do while loop
    // int i = 1;
    // do
    // {
    //     cout << i << endl;
    //     i++;
    // } while (false);

    // Quiz: Make a multiplication table of 6
    // for (int i = 1; i <= 10; i++)
    // {
    //     cout << "6 x " << i << " = " << 6 * i << endl;
    // }

    // int i = 1;
    // while (i <= 10)
    // {
    //     cout << "6 x " << i << " = " << 6 * i << endl;
    //     i++;
    // }

    int i = 1;
    do
    {
        cout << "6 x " << i << " = " << 6 * i << endl;
        i++;
    } while (i <= 10);

    return 0;
}