// This program was created by Harshit Nagpal
#include <iostream>
using namespace std;

// This is a global variable. Global variable is declared outside a function
int glo = 6; 

// This is a function. We will study this later.
void sum()
{
    int a;
    cout << glo;
}

int main()
{
    // Local variable takes precendence over a global variable
    int glo = 9;

    // int a = 14;
    // int b = 15;
    // You can also write the above two lines as this:
    int a = 14, b = 15;

    // Bool shows 0 for false and 1 for true
    bool is_true = false;

    // endl and \n can be used to add a new line
    cout << glo << is_true << endl;
    return 0;
}