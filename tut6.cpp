// This program was created by Harshit Nagpal
#include <iostream>
using namespace std;

int main()
{
    // There are two types of header files:
    // 1. System header files: It comes with the compiler
    // 2. User defined header files: It is written by the programmer

    int a = 4, b = 5;

    cout << "Operators in C++:" << endl;
    cout << "Following are the types of operators in C++" << endl;

    // Arithmetic operators
    cout << "The value of a + b is : " << a + b << endl;
    cout << "The value of a - b is : " << a - b << endl;
    cout << "The value of a * b is : " << a * b << endl;
    cout << "The value of a / b is : " << a / b << endl;
    cout << "The value of a % b is : " << a % b << endl;

    // Some other arithmetic operators
    cout << "The value of a++ is : " << a++ << endl;
    cout << "The value of a-- is : " << a-- << endl;
    cout << "The value of ++a is : " << ++a << endl;
    cout << "The value of --a is : " << --a << endl;

    // Assignment Operators are used to assign values to variables
    // int a = 3, b = 9;

    // Comparison operators
    cout << "The value of a == b is : " << (a == b) << endl;
    cout << "The value of a != b is : " << (a != b) << endl;
    cout << "The value of a >= b is : " << (a >= b) << endl;
    cout << "The value of a <= b is : " << (a <= b) << endl;

    // Some other comparison operators
    cout << "The value of a > b is : " << (a > b) << endl;
    cout << "The value of a < b is : " << (a < b) << endl;

    // Logical operators
    cout << "The value of((a==b) && (a<b)) is : " << ((a == b) && (a < b)) << endl;
    // The above line contains the use of Logical and
    cout << "The value of((a==b) || (a<b)) is : " << ((a == b) || (a < b)) << endl;
    // The above line contains the use of Logical or
    cout << "The value of(!(a==b)) is : " << (!(a == b)) << endl;
    // The above line contains the use of Logical not

    return 0;
}