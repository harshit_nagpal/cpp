// This program was created by Harshit Nagpal
#include <cmath>
#include <iostream>
using namespace std;

class Point
{
    int x, y;

public:
    Point(int a, int b)
    {
        x = a;
        y = b;
    }

    void displayPoint()
    {
        cout << "The point is : (" << x << ", " << y << ")" << endl;
    }
    friend void distPoint(Point p, Point q);
};

// Create a function (Hint: Make it a friend function) which takes 2 point objects and computes the distance between those 2 points.

// Use these examples to check your code:
// Distance between (1, 1) and (1, 1) is : 0  Done!
// Distance between (0, 1) and (0, 6) is : 5  Done!
// Distance between (1, 0) and (7, 0) is : 6  Done!

void distPoint(Point p, Point q)
{
    int ans = sqrt(((q.x - p.x) * (q.x - p.x)) + ((q.y - p.y) * (q.y - p.y)));
    cout << "The distance between these two points is : " << ans << endl;
}

int main()
{
    // Point p(1, 1);
    // p.displayPoint();

    // Point q(4, 6);
    // q.displayPoint();

    // For the quiz
    Point n1(1, 0);
    n1.displayPoint();

    Point n2(7, 0);
    n2.displayPoint();

    distPoint(n1, n2);

    return 0;
}