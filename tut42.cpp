#include <iostream>
#include <string>
#include <cmath>
using namespace std;

/*
Create 2 classes:
    1. SimpleCalculator - Takes input of 2 numbers using a utility function and performs +, -, *, / and displays the results using another function.
    2. ScientificCalculator - Takes input of 2 numbers using a utility function and performs any four scientific operations of your choice and displays the results using another function.

    Create another class HybridCalculator and inherit it using these 2 classes:
    Q1. What type of Inheritance are you using?
    A:  Multiple
    Q2. Which mode of Inheritance are you using?
    A:  Public
    Q3. Create an object of HybridCalculator and display results of the simple and scientific calculator.
    A:  Done below
    Q4. How is code reusability implemented?
    A:  By using inheritance
*/

class simpleCalculator
{
public:
    int a, b;
    void calc1_input(float x, float y)
    {
        a = x;
        b = y;
    }
    void calc1_operation(char c)
    {
        if (c == '+')
        {
            cout << "The ans is : " << a + b << endl;
        }
        else if (c == '-')
        {
            cout << "The ans is : " << a - b << endl;
        }
        else if (c == '*')
        {
            cout << "The ans is : " << a * b << endl;
        }
        else if (c == '/')
        {
            cout << "The ans is : " << a / b << endl;
        }
        else
        {
            cout << "Incorrect Operator\n";
        }
    };
};

class scientificCalculator
{
public:
    float x;
    void calc2_input(float y)
    {
        x = y;
    }
    void calc2_operation(string s)
    {
        if (s == "sqrt")
        {
            cout << sqrt(x) << endl;
        }
        else if (s == "cbrt")
        {
            cout << cbrt(x) << endl;
        }
        else if (s == "sin")
        {
            cout << sin(x) << endl;
        }
        else if (s == "cos")
        {
            cout << cos(x) << endl;
        }
        else if (s == "tan")
        {
            cout << tan(x) << endl;
        }
        else
        {
            cout << "Incorrect Operation\n";
        }
    }
};

class hybridCalculator : public simpleCalculator, public scientificCalculator
{
};

int main()
{
    hybridCalculator h1;
    int a, b;
    char c;

    cout << "Enter the values of a and b : ";
    cin >> a >> b;
    cout << "Enter the Operator : ";
    cin >> c;

    h1.calc1_input(a, b);
    h1.calc1_operation(c);

    hybridCalculator h2;
    int v;
    string s;

    cout << "Enter the value : ";
    cin >> v;
    cout << "Enter the operation : ";
    cin >> s;

    h2.calc2_input(v);
    h2.calc2_operation(s);

    return 0;
}