// This program was created by Harshit Nagpal
#include <iostream>
using namespace std;

typedef struct employee
{
    int nophones;
    char favchar;
    float salary;
} ep;

union money
{
    int rice;
    char car;
    float tv;
};

int main()
{
    enum Meal
    {
        breakfast,
        lunch,
        dinner
    };
    Meal m1 = lunch;
    cout << (m1 == 2) << endl;

    // union money m1;
    // m1.rice = 36;
    // m1.car = 'c';
    // cout << m1.car << endl;

    // ep harry;
    // struct employee shubham;
    // struct employee rohanDas;
    // harry.nophones = 1;
    // harry.favchar = 'c';
    // harry.salary = 12000;

    // cout << "The value is: " << harry.nophones << endl;
    // cout << "The value is: " << harry.favchar << endl;
    // cout << "The value is: " << harry.salary << endl;
    return 0;
}