// This program was created by Harshit Nagpal
#include <iostream>
using namespace std;

// Call by value. Only copy of actual parameters is made
// void square(int n)
// {
//     n = n * n;
//     cout << "Value in Function : " << n << endl;
// }

// Call by reference using reference variable : &. A reference to actual parameters is made
void square(int &n)
{
    n = n * n;
    cout << "Value in Function : " << n << endl;
}

// Call by reference using reference variable : &.
void swapPointer(int *a, int *b)
{                  // temp a  b
    int temp = *a; // 4    4  5
    *a = *b;       // 4    5  5
    *b = temp;     // 4    5  4
}

int main()
{
    int n, x = 4, y = 5;
    cout << "Enter value : ";
    cin >> n;
    square(n);
    cout << "Value in Main : " << n << endl;

    swapPointer(&x, &y);
    cout << "The value of x after swap using pointer is : " << x << endl;
    cout << "The value of y after swap using pointer is : " << y << endl;
    return 0;
}