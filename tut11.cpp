// This program was created by Harshit Nagpal
#include <iostream>
using namespace std;

int main()
{
    // Break helps to exit the loop
    // for (int i = 0; i < 40; i++)
    // {
    //     if (i == 2)
    //     {
    //         break;
    //     }
    //     cout << i << endl;
    // }

    // Continue helps to skip the current iteration of the loop
    for (int i = 0; i < 40; i++)
    {
        if (i == 2)
        {
            continue;
        }
        cout << i << endl;
    }

    return 0;
}